Building a Docker image for Evince CI
-------------------------------------

```
docker login registry.gitlab.gnome.org
```

```
$ docker build -t registry.gitlab.gnome.org/gnome/evince/master-amd64 -f ubuntu-Dockerfile .
$ docker push registry.gitlab.gnome.org/gnome/evince/master-amd64
```
